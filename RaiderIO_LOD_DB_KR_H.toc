## Interface: 80205
## Author: cqwrteur
## Title: Raider.IO LOD Database KR Horde
## Version: @project-version@
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 2
## X-RAIDER-IO-LOD-FACTION: Horde

db/db_kr_horde_characters.lua
db/db_kr_horde_lookup.lua
